package com.mindtree.vibgyorpaints.imp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.CannotFindFileException;
import com.mindtree.vibgyorpaints.exceptions.FormatNotValidException;
import com.mindtree.vibgyorpaints.exceptions.InputOutputException;
import com.mindtree.vibgyorpaints.locators.ForgotPasswordLocaters;
import com.mindtree.vibgyorpaints.locators.LoginPageLocators;

public class ForgotPassword extends TestBase{

	String sheetname="Sheet1";
	public ForgotPassword() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<String> getforgotPasswordAction() throws CannotFindFileException, FormatNotValidException, InputOutputException
	{
		ArrayList<String> actions=null;
		
		
				try {
					actions = ForgotPasswordLocaters.getTestData(sheetname);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					throw new CannotFindFileException("File not Found"+e.getMessage());
					
				} catch (InvalidFormatException e) {
					// TODO Auto-generated catch block
					throw new FormatNotValidException("Invalid Format"+e.getMessage());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					throw new InputOutputException("IO Exception occurred"+e.getMessage());
				}
			
		
		return actions;
	}
	public ArrayList<String> getforgotPasswordLocator(){
		ArrayList<String> locator=new ArrayList<String>();
		locator.add(ForgotPasswordLocaters.login);
		locator.add(ForgotPasswordLocaters.forgotPasswordbutton);
		locator.add(ForgotPasswordLocaters.emailid);
		locator.add(ForgotPasswordLocaters.sendMail);
		locator.add(ForgotPasswordLocaters.popUpMessage);
		return locator;
	}

	public String retrievePassword() {
		// TODO Auto-generated method stub
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1)); 
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession");
		driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys("vikas12353@gmail.com");
		driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
		driver.findElement(By.xpath("//input[@aria-label='Enter your password']")).sendKeys("yuvakishore");
		driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
		String res=driver.findElement(By.xpath("//span[@email='vibgyor.feb2018@gmail.com']/ancestor::td/following-sibling::td//span[contains(text(),'Hello')]")).getText();
		String pass=res.substring(res.length()-9);
		System.out.println(pass);
		driver.close();
		driver.switchTo().window(tabs.get(0)); 
		return pass;
	}
}
