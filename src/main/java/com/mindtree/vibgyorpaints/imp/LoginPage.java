package com.mindtree.vibgyorpaints.imp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.CannotFindFileException;
import com.mindtree.vibgyorpaints.exceptions.ElementNotClickable;
import com.mindtree.vibgyorpaints.exceptions.ElementNotPresentException;
import com.mindtree.vibgyorpaints.exceptions.FormatNotValidException;
import com.mindtree.vibgyorpaints.exceptions.InputOutputException;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;
import com.mindtree.vibgyorpaints.interfaces.LoginPageInterface;
import com.mindtree.vibgyorpaints.locators.*;
import com.mindtree.vibgyorpaints.utils.TestUtil;

public class LoginPage extends TestBase implements LoginPageInterface
{
	
	String sheetname="Sheet1";

	public ArrayList<String> getLoginAction() throws CannotFindFileException, FormatNotValidException, InputOutputException
	{
		ArrayList<String> actions=null;
		
		
				try {
					actions = LoginPageLocators.getTestData(sheetname);
				} catch (FileNotFoundException e) {
					throw new CannotFindFileException("File not Found"+e.getMessage());
					
				} catch (InvalidFormatException e) {
					throw new FormatNotValidException("Invalid Format"+e.getMessage());
				} catch (IOException e) {
					throw new InputOutputException("IO Exception occurred"+e.getMessage());
				}
			
		
		return actions;
	}
	
	public ArrayList<String> getLoginLocator(){
		ArrayList<String> locator=new ArrayList<String>();
		locator.add(LoginPageLocators.login);
		locator.add(LoginPageLocators.emailid);
		locator.add(LoginPageLocators.password);
		locator.add(LoginPageLocators.loginbuton);
		return locator;
	}
	
	public String validateLogin() throws LocatorNotFound,ElementNotClickable,ElementNotPresentException
	{
		String str;
		try {
			 WebDriverWait wait=new WebDriverWait(driver, 30);
				str = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='swal2-content']"))).getText();
			//str = driver.findElement(By.xpath("//div[@id='swal2-content']")).getText();
			//driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		} catch (NoSuchElementException e) {
			throw new LocatorNotFound("Locator not found");
			
		}
		catch (ElementNotSelectableException e) {
			throw new ElementNotClickable("Unable to find element"+e.getMessage());
		}
		
			catch(StaleElementReferenceException e)
			{
				throw new ElementNotPresentException("element is no longer appearing on the DOM page"+e.getMessage());
			}
		return str;
	}
		
	}

	

	

	


