package com.mindtree.vibgyorpaints.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.ElementNotClickable;
import com.mindtree.vibgyorpaints.exceptions.ElementNotPresentException;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;

public class CommonMethod extends TestBase
{
	
	public static void sendKeys(String strLocValue, String param)
			throws InterruptedException, LocatorNotFound, ElementNotClickable, ElementNotPresentException {

		try {
			 WebDriverWait wait=new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(strLocValue))).sendKeys(param);
			//driver.findElement(By.xpath(strLocValue)).sendKeys(param);
			Thread.sleep(1000);
		} catch (NoSuchElementException e) {
			throw new LocatorNotFound("Cannot Find Element" + e.getMessage());
		} catch (ElementNotSelectableException e) {
			throw new ElementNotClickable("Unable to find element" + e.getMessage());
		}

		catch (StaleElementReferenceException e) {
			throw new ElementNotPresentException("element is no longer appearing on the DOM page" + e.getMessage());
		}

	}

	public static void click_element(String strLocValue)
			throws InterruptedException, LocatorNotFound, ElementNotClickable, ElementNotPresentException {

		try {
			

			 WebDriverWait wait=new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(strLocValue))).click();
			
			//driver.findElement(By.xpath(strLocValue)).click();
			Thread.sleep(1000);
		} catch (NoSuchElementException e) {
			throw new LocatorNotFound("Cannot Find Element" + e.getMessage());
		} catch (ElementNotSelectableException e) {
			throw new ElementNotClickable("Unable to find element" + e.getMessage());
		}

		catch (StaleElementReferenceException e) {
			throw new ElementNotPresentException("element is no longer appearing on the DOM page" + e.getMessage());
		}

	}

	public static boolean islabelpresent(String strLocValue)
			throws InterruptedException, LocatorNotFound, ElementNotClickable, ElementNotPresentException {

		try {
			return driver.findElement(By.xpath(strLocValue)).isDisplayed();
		} catch (NoSuchElementException e) {
			throw new LocatorNotFound("Cannot Find Element" + e.getMessage());
		} catch (ElementNotSelectableException e) {
			throw new ElementNotClickable("Unable to find element" + e.getMessage());
		}

		catch (StaleElementReferenceException e) {
			throw new ElementNotPresentException("element is no longer appearing on the DOM page" + e.getMessage());
		}

	}

	public static String gettext(String strLocValue)
			throws InterruptedException, LocatorNotFound, ElementNotClickable, ElementNotPresentException {
		try {
			 WebDriverWait wait=new WebDriverWait(driver, 30);
		String str =	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(strLocValue))).getText();
		return str;
			//return driver.findElement(By.xpath(strLocValue)).getText();
		}

		catch (NoSuchElementException e) {
			throw new LocatorNotFound("Cannot Find Element" + e.getMessage());
		} catch (ElementNotSelectableException e) {
			throw new ElementNotClickable("Unable to find element" + e.getMessage());
		}

		catch (StaleElementReferenceException e) {
			throw new ElementNotPresentException("element is no longer appearing on the DOM page" + e.getMessage());
		}
	}
   
}
