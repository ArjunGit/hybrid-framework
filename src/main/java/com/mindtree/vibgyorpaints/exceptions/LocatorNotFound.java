package com.mindtree.vibgyorpaints.exceptions;


public class LocatorNotFound extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LocatorNotFound(String s) {
		super(s);
	}

}
