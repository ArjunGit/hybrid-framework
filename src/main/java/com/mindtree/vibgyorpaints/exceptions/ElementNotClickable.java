package com.mindtree.vibgyorpaints.exceptions;

public class ElementNotClickable extends Exception
{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public ElementNotClickable(String s) {
	super(s);
}
}
