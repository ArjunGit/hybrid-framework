package com.mindtree.vibgyorpaints.locators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.mindtree.vibgyorpaints.base.TestBase;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ForgotPasswordLocaters extends TestBase{
	public static String login;
	public  static String forgotPasswordbutton;
	public static  String emailid;
	public static  String sendMail;
	public static  String popUpMessage;
	
	
	
	
	public static String TESTDATA_SHEET_PATH =System.getProperty("user.dir")+"/src/main/java/com/mindtree/vibgyorpaints/resources/VibyorSampleTestData.xlsx";

	static Workbook book;
	static Sheet sheet;

	public static ArrayList<String> getTestData(String sheetName) throws FileNotFoundException,InvalidFormatException, IOException {
		FileInputStream file = null;
	
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		
			book = WorkbookFactory.create(file);
		
		
		sheet = book.getSheet(sheetName);
		
		String tc;String parameter;
	
		ArrayList<String> al=new ArrayList<String>();
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
		
				tc = sheet.getRow(i + 1).getCell(0).toString();
				parameter=sheet.getRow(i + 1).getCell(3).toString();
				
				if(tc.equals("forgotPassword"))
				{   if(!(sheet.getRow(i + 1).getCell(4).toString()).equals("navigate"))
					{
					al.add(sheet.getRow(i + 1).getCell(4).toString());
					}
					if(parameter.equals("login_button"))
					{
						login=sheet.getRow(i + 1).getCell(2).toString();
						
						//System.out.println(login);
					}
					else if(parameter.equals("email_id"))
					{
						emailid=sheet.getRow(i + 1).getCell(2).toString();
						//System.out.println(emailid);
					}
					else if(parameter.equals("forgot_button"))
					{
						forgotPasswordbutton=sheet.getRow(i + 1).getCell(2).toString();
						//System.out.println(password);
					}
					else if(parameter.equals("send_mail"))
					{
						sendMail=sheet.getRow(i + 1).getCell(2).toString();
						//System.out.println(loginbuton);
					}
					else if(parameter.equals("popUp_message"))
					{
						popUpMessage=sheet.getRow(i + 1).getCell(2).toString();
						//System.out.println(loginbuton);
					}
					
					
			}
		
	}
		return al;
	

	
	
	
}
}
