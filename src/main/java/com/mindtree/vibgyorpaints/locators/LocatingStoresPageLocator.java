package com.mindtree.vibgyorpaints.locators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class LocatingStoresPageLocator 
{

	public static  String locatingStore;
	public static  String bangaloreStore;
	public static  String hyderabadStore;
	public static  String mumbaiStore;
	public static  String chennaiStore;
	public static  String kolkataStore;
	public static  String storeInBangalore;

	public static  String storeInHyderabad;

	public static  String storeInMumbai;
	public static  String storeInChennai;
	public static  String storeInKolkata;
	
	
	static int firstcolumn=1;
	static int secondcolumn=2;
	static int thirdcolumn=3;
	static int fourthcolumn=4;
	
	
	public static String TESTDATA_SHEET_PATH =System.getProperty("user.dir")+"/src/main/java/com/mindtree/vibgyorpaints/resources/VibyorSampleTestData.xlsx";
	static Workbook book;
	static Sheet sheet;

	public static ArrayList<String> getTestData(String sheetName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sheet = book.getSheet(sheetName);
		
		String tc;String parameter;
	
		ArrayList<String> al=new ArrayList<String>();
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
		
				tc = sheet.getRow(i + firstcolumn).getCell(0).toString();
				parameter=sheet.getRow(i + firstcolumn).getCell(thirdcolumn).toString();
				
				if(tc.equals("locatingstore"))
				{   if(!(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString()).equals("navigate"))
					{
					al.add(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString());
					}
					if(parameter.equals("locatingstoreicon"))
					{
						locatingStore=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					else if(parameter.equals("banglorestore"))
					{
						bangaloreStore=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					else if(parameter.equals("banglorepaint"))
					{
						storeInBangalore=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					
					else if(parameter.equals("hyderabadstore"))
					{
						hyderabadStore=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
						//System.out.println(hyderabadStore);
					}
					else if(parameter.equals("hyderabadpaint"))
					{
						storeInHyderabad=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					
					else if(parameter.equals("mumbaistore"))
					{
						mumbaiStore=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					else if(parameter.equals("mumbaipaint"))
					{
						storeInMumbai=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					
					else if(parameter.equals("chennaistore"))
					{
						chennaiStore=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					else if(parameter.equals("chennaipaint"))
					{
						storeInChennai=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					
					
					else if(parameter.equals("kolkatastore"))
					{
						kolkataStore=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					else if(parameter.equals("kolkatapaint"))
					{
						storeInKolkata=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					
					
			}
		
	}
		return al;
	

	
	
	
}
	
	

}

