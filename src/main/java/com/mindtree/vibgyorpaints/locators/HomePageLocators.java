package com.mindtree.vibgyorpaints.locators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class HomePageLocators {
	
	public static  String vibgyorPaintLabel;
	public static  String loginLabel;
	public static String signUpLabel;
	
	
	static int firstcolumn=1;
	static int secondcolumn=2;
	static int thirdcolumn=3;
	static int fourthcolumn=4;
	
	public static String TESTDATA_SHEET_PATH =System.getProperty("user.dir")+"/src/main/java/com/mindtree/vibgyorpaints/resources/VibyorSampleTestData.xlsx";

	static Workbook book;
	static Sheet sheet;
	
	public static ArrayList<String> getTestData(String sheetName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sheet = book.getSheet(sheetName);
		
		String tc;String parameter;
		
		ArrayList<String> al=new ArrayList<String>();
		
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			
			
				tc = sheet.getRow(i + firstcolumn).getCell(0).toString();
				parameter=sheet.getRow(i + firstcolumn).getCell(thirdcolumn).toString();
				if(tc.equals("homepage"))
				{
					 if(!(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString()).equals("navigate"))
						{
						al.add(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString());
						}
					
					if(parameter.equals("vibgyor_label"))
					{
						vibgyorPaintLabel=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					else if(parameter.equals("login_label"))
					{
						loginLabel=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					else if(parameter.equals("signup_label"))
					{
						signUpLabel=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					}
					
					
				
			}
		
	}
		return al;
	

}
}
