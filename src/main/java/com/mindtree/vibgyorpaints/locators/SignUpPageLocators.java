package com.mindtree.vibgyorpaints.locators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class SignUpPageLocators {

	public static String clickSignup;

	public static String emailId;

	public static String password;

	public static String confirmPassword;

	public static String userName;

	public static String location;

	public static String phoneNumber;

	public static String signUp;

	public static String signUpOk = "//button[@class='swal2-confirm swal2-styled']";
	
	
	static int firstcolumn=1;
	static int secondcolumn=2;
	static int thirdcolumn=3;
	static int fourthcolumn=4;

	public static String TESTDATA_SHEET_PATH =System.getProperty("user.dir")+"/src/main/java/com/mindtree/vibgyorpaints/resources/VibyorSampleTestData.xlsx";

	static Workbook book;
	static Sheet sheet;

	public static ArrayList<String> getTestData(String sheetName)
			throws FileNotFoundException, InvalidFormatException, IOException {
		FileInputStream file = null;

		file = new FileInputStream(TESTDATA_SHEET_PATH);

		book = WorkbookFactory.create(file);

		sheet = book.getSheet(sheetName);

		String tc;
		String parameter;

		ArrayList<String> al = new ArrayList<String>();
		for (int i = 0; i < sheet.getLastRowNum(); i++) {

			tc = sheet.getRow(i + firstcolumn).getCell(0).toString();
			parameter = sheet.getRow(i + firstcolumn).getCell(thirdcolumn).toString();

			if (tc.equals("signup")) {
				if (!(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString()).equals("navigate")) {
					al.add(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString());
				}
				if (parameter.equals("signupbutton")) {
					clickSignup = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					// System.err.println(clickSignup);
				} else if (parameter.equals("email_id")) {
					emailId = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
					// System.err.println(emailId);
				} else if (parameter.equals("password")) {
					password = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
				} else if (parameter.equals("confirmpassword")) {
					confirmPassword = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
				} else if (parameter.equals("fullname")) {
					userName = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
				} else if (parameter.equals("location")) {
					location = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
				} else if (parameter.equals("mobnum")) {
					phoneNumber = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
				} else if (parameter.equals("signupsubmit")) {
					signUp = sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
				}

			}

		}
		return al;

	}
}
