package com.mindtree.vibgyorpaints.locators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class LoginPageLocators 
{
	
	public static String login;
	public static  String emailid;
	public static  String password;
	public  static String loginbuton;
	
	
	
	
	static int firstcolumn=1;
	static int secondcolumn=2;
	static int thirdcolumn=3;
	static int fourthcolumn=4;
	
	
	
	
	
	public static String TESTDATA_SHEET_PATH =System.getProperty("user.dir")+"/src/main/java/com/mindtree/vibgyorpaints/resources/VibyorSampleTestData.xlsx";

	static Workbook book;
	static Sheet sheet;

	public static ArrayList<String> getTestData(String sheetName) throws FileNotFoundException,InvalidFormatException, IOException {
		FileInputStream file = null;
	
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		
			book = WorkbookFactory.create(file);
		
		
		sheet = book.getSheet(sheetName);
		
		String tc;String parameter;
	
		ArrayList<String> al=new ArrayList<String>();
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
		
				tc = sheet.getRow(i + firstcolumn).getCell(0).toString();
				parameter=sheet.getRow(i + firstcolumn).getCell(thirdcolumn).toString();
				
				if(tc.equals("login"))
				{   if(!(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString()).equals("navigate"))
					{
					al.add(sheet.getRow(i + firstcolumn).getCell(fourthcolumn).toString());
					}
					if(parameter.equals("login_button"))
					{
						login=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
						
						//System.out.println(login);
					}
					else if(parameter.equals("email_id"))
					{
						emailid=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
						//System.out.println(emailid);
					}
					else if(parameter.equals("password"))
					{
						password=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
						//System.out.println(password);
					}
					else if(parameter.equals("submit_button"))
					{
						loginbuton=sheet.getRow(i + firstcolumn).getCell(secondcolumn).toString();
						//System.out.println(loginbuton);
					}
					
			}
		
	}
		return al;
	

	
	
	
}
}
