package com.mindtree.vibgyorpaints.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mindtree.vibgyorpaints.utils.TestUtil;
import com.mindtree.vibgyorpaints.utils.WebEventListener;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;



public class TestBase 
{
	public static WebDriver driver;
	public static Properties prop;
	public  static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static ExtentReports report=new ExtentReports(System.getProperty("user.dir")+"/test-output/Vibgyor paint Automation Script/VibgyorExtentReport.html");
	public static ExtentTest logger;
	public static Logger log =Logger.getLogger("VibgyorPaintsLog"); 
	/*public static ChromeOptions options;
	public static DesiredCapabilities capabilities;*/
	
	
	public TestBase() {
		try {
			
			prop = new Properties();
			PropertyConfigurator.configure(System.getProperty("user.dir")+"/test-output/log4j.properties"); 
			FileInputStream ip=new FileInputStream(System.getProperty("user.dir")+"/src/main/java/com/mindtree/vibgyorpaints/configs/config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	public static void initialization(){
		String browserName = prop.getProperty("browser");
		
		if(browserName.equals("chrome")){
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "/src/main/java/com/mindtree/vibgyorpaints/resources/chromedriver.exe");	
//			options=new ChromeOptions();
//			capabilities=DesiredCapabilities.chrome();
//			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//			options.setHeadless(true);
//			options.addArguments("<---ignore certificate erros-->");
//			driver=new ChromeDriver(options);
			driver = new ChromeDriver(); 
			
		}
		else if(browserName.equals("IE")){
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"/src/main/java/com/mindtree/vibgyorpaints/resources/IEDriverServer.exe");
			driver = new FirefoxDriver(); 
		}
		
		
		e_driver = new EventFiringWebDriver(driver);
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("url"));

}
}
