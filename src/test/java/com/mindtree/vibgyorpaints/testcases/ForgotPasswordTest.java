package com.mindtree.vibgyorpaints.testcases;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.CannotFindFileException;
import com.mindtree.vibgyorpaints.exceptions.ElementNotClickable;
import com.mindtree.vibgyorpaints.exceptions.ElementNotPresentException;
import com.mindtree.vibgyorpaints.exceptions.FormatNotValidException;
import com.mindtree.vibgyorpaints.exceptions.InputOutputException;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;
import com.mindtree.vibgyorpaints.imp.ForgotPassword;
import com.mindtree.vibgyorpaints.imp.LoginPage;
import com.mindtree.vibgyorpaints.utils.CommonMethod;
import com.mindtree.vibgyorpaints.utils.ScreenShot;
import com.relevantcodes.extentreports.LogStatus;

public class ForgotPasswordTest extends TestBase{
	ForgotPassword forgotPassword;
	ArrayList<String> al;
	ArrayList<String> kk;

	public ForgotPasswordTest() {
		// TODO Auto-generated constructor stub
		super();
	}
	@BeforeMethod
	
	public void setUp(){
		initialization();
		forgotPassword = new ForgotPassword();

	}
	@Test
	public void forgotPasswordTest()
	{
		logger = report.startTest("Forgot Password Test");
		String actualValue = null;
		try {
			al = forgotPassword.getforgotPasswordAction();
			kk = forgotPassword.getforgotPasswordLocator();
		} catch (CannotFindFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FormatNotValidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InputOutputException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int j = 0;

		for (int i = 0; i < al.size(); i++) {
			switch (al.get(i)) {
			case "click":

				try {
					CommonMethod.click_element(kk.get(i));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LocatorNotFound e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ElementNotClickable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ElementNotPresentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case "sendkeys":
				try {
					CommonMethod.sendKeys(kk.get(i), "vikas12353@gmail.com");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LocatorNotFound e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ElementNotClickable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ElementNotPresentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				j++;
				break;
			case "gettext":
				try {
					actualValue = CommonMethod.gettext(kk.get(i));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LocatorNotFound e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ElementNotClickable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ElementNotPresentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			}

			
		}
		Assert.assertEquals(actualValue, "Email sent successfully to your Email Id","User not Registered");
		String realPassword=forgotPassword.retrievePassword();
		System.out.println(realPassword);

		
	}
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if (result.getStatus() == ITestResult.FAILURE) {
			String imagePath = ScreenShot.captureScreenShot(driver, result.getName());
			String image = logger.addScreenCapture(imagePath) + "Some error occurred in Login";
			logger.log(LogStatus.ERROR, image);
			log.error("cannot send email");

		}

		report.endTest(logger);
		report.flush();

		Thread.sleep(3000);
		driver.quit();
	}

}
