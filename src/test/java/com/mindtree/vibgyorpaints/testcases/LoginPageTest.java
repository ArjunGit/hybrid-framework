package com.mindtree.vibgyorpaints.testcases;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.CannotFindFileException;
import com.mindtree.vibgyorpaints.exceptions.ElementNotClickable;
import com.mindtree.vibgyorpaints.exceptions.ElementNotPresentException;
import com.mindtree.vibgyorpaints.exceptions.FormatNotValidException;
import com.mindtree.vibgyorpaints.exceptions.InputOutputException;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;
import com.mindtree.vibgyorpaints.imp.LoginPage;
import com.mindtree.vibgyorpaints.utils.CommonMethod;
import com.mindtree.vibgyorpaints.utils.ScreenShot;
import com.mindtree.vibgyorpaints.utils.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPageTest extends TestBase {

	LoginPage loginpage;
	String sheetName = "Login";
	ArrayList<String> al;
	ArrayList<String> kk;
	int testCount = 1;

	public LoginPageTest() {

		super();

	}

	@BeforeMethod
	public void setUp() {
		initialization();
		loginpage = new LoginPage();

	}

	@DataProvider(name = "VibgyorpaintTestData")
	public Object[][] getTestData() {
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}

	@Test(priority = 1, dataProvider = "VibgyorpaintTestData")
	public void loginPageTest(String email, String pwd,String ExpectedResult)  {

		String actualValue = null;
		logger = report.startTest("Login page test for user:" + testCount);
		log.info("Login page test");
		logger.log(LogStatus.INFO, "Verifying Login credentials");
		log.info("Verifying Login credentials for user:" + testCount);

		try {
			al = loginpage.getLoginAction();

			kk = loginpage.getLoginLocator();

			ArrayList<String> ff = new ArrayList<String>();
			ff.add(email);
			ff.add(pwd);
			int j = 0;

			for (int i = 0; i < al.size(); i++) {
				switch (al.get(i)) {
				case "click":

					CommonMethod.click_element(kk.get(i));
					break;
				case "sendkeys":
					CommonMethod.sendKeys(kk.get(i), ff.get(j));
					j++;
					break;

				}

				
			}
			actualValue = loginpage.validateLogin();
			testCount++;
			Assert.assertEquals(actualValue,ExpectedResult, "login funtionality failed");
			logger.log(LogStatus.PASS, "Login funtionality passed");
			log.info("login funtionality working properly");

		} catch (ElementNotClickable e) {
			log.fatal(e.getMessage());
		} catch (ElementNotPresentException e) {
			log.fatal(e.getMessage());
		} catch (LocatorNotFound e) {
			log.fatal("unable to find element");
			logger.log(LogStatus.ERROR, "Unable to find element");
		} catch (CannotFindFileException e1) {
			log.fatal(e1.getMessage());
		} catch (FormatNotValidException e1) {
			log.fatal(e1.getMessage());
		} catch (InputOutputException e1) {
			log.fatal(e1.getMessage());
		}catch(Exception e)
		{
			log.fatal(e.getMessage());
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if (result.getStatus() == ITestResult.FAILURE) {
			String imagePath = ScreenShot.captureScreenShot(driver, result.getName());
			String image = logger.addScreenCapture(imagePath) + "Some error occurred in Login";
			logger.log(LogStatus.ERROR, image);
			log.error("login funtionality failed");

		}

		report.endTest(logger);
		report.flush();

		Thread.sleep(3000);
		driver.close();
	}
}
