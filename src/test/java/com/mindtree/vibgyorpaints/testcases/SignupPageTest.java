package com.mindtree.vibgyorpaints.testcases;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.CannotFindFileException;
import com.mindtree.vibgyorpaints.exceptions.ElementNotClickable;
import com.mindtree.vibgyorpaints.exceptions.ElementNotPresentException;
import com.mindtree.vibgyorpaints.exceptions.FormatNotValidException;
import com.mindtree.vibgyorpaints.exceptions.InputOutputException;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;
import com.mindtree.vibgyorpaints.imp.SignUp;
import com.mindtree.vibgyorpaints.utils.CommonMethod;
import com.mindtree.vibgyorpaints.utils.ScreenShot;
import com.mindtree.vibgyorpaints.utils.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class SignupPageTest extends TestBase {
	String sheetName = "SignUp";
	SignUp signup;
	ArrayList<String> al;
	ArrayList<String> kk;
	int testCount=1;

	public SignupPageTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		signup = new SignUp();

	}

	@DataProvider(name = "SignUpTestdata")
	public Object[][] TestSignUpData() {

		Object[][] data = TestUtil.getTestData(sheetName);

		return data;

	}

	@Test(priority=2,dataProvider = "SignUpTestdata")
	public void filSignUp(String emailId, String password, String confirmPassword, String userName, String location,
			String phoneNo,String ExpectedResult) {
		try {
			logger = report.startTest("SignUp test for user:" + testCount);
			log.info("SignUp page test for user:"+testCount);
			logger.log(LogStatus.INFO, "Verifying Sign Up credentials");
			log.info("Verifying Login credentials for user:" + testCount++);
			
			
			al = signup.getSignUpAction();
			//System.out.println(al);
			kk = signup.getSignUpLocator();
			//System.out.println(kk);

			ArrayList<String> ff = new ArrayList<>();
			ff.add(emailId);
			ff.add(password);
			ff.add(confirmPassword);
			ff.add(userName);
			ff.add(location);
			ff.add(phoneNo);
			int j = 0;


			for (int i = 0; i < al.size(); i++) {
				switch (al.get(i)) {
				case "click":
					CommonMethod.click_element(kk.get(i));
					break;
				case "sendkeys":
					CommonMethod.sendKeys(kk.get(i), ff.get(j));
					j++;
					break;

				}
			}

			

			
			String actualvalue = signup.verifysignup();
			Assert.assertEquals(actualvalue,ExpectedResult, "Sign up funtionality failed");
			logger.log(LogStatus.PASS, "Registration funtionality sucessful");
			log.info("Registration funtionality sucessful");
		} catch (ElementNotClickable e) {
			Assert.assertTrue(true);
			log.fatal(e.getMessage());
		} catch (ElementNotPresentException e) {
			Assert.assertTrue(true);
			log.fatal(e.getMessage());
		} catch (LocatorNotFound e) {
			Assert.assertTrue(true);
			log.fatal("unable to find element");
			logger.log(LogStatus.ERROR, "Unable to find element");
		} catch (CannotFindFileException e1) {
			log.fatal(e1.getMessage());
		} catch (FormatNotValidException e1) {
			log.fatal(e1.getMessage());
		} catch (InputOutputException e1) {
			log.fatal(e1.getMessage());
		}
		catch(Exception e)
		{
			log.fatal(e.getMessage());
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if (result.getStatus() == ITestResult.FAILURE) {
			String imagePath = ScreenShot.captureScreenShot(driver, result.getName());
			String image = logger.addScreenCapture(imagePath) + "Some error occurred in SignUp";
			logger.log(LogStatus.ERROR, image);
			log.info("Registration funtionality Failed");
			

		}
		report.endTest(logger);
		report.flush();

		Thread.sleep(3000);
		driver.quit();
	}

}
