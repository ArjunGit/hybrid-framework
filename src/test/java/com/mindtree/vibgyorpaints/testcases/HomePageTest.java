package com.mindtree.vibgyorpaints.testcases;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.ElementNotClickable;
import com.mindtree.vibgyorpaints.exceptions.ElementNotPresentException;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;
import com.mindtree.vibgyorpaints.imp.HomePage;
import com.mindtree.vibgyorpaints.utils.CommonMethod;
import com.mindtree.vibgyorpaints.utils.ScreenShot;
import com.relevantcodes.extentreports.LogStatus;

public class HomePageTest extends TestBase 
{

	HomePage homepage;
	ArrayList<String> al;
	ArrayList<String> kk;

	int i=0;
	int count=1;

	public HomePageTest() 
	{
		super();

	} 


	@BeforeMethod
	public void setUp(){
		initialization();
		homepage = new HomePage();


		al = homepage.getHomepageAction();
		kk = homepage.getHomePageLocator();
	}



	@Test(priority=4)
	public void HomePageTitleTest()
	{
		logger=report.startTest("HomePageTitleTest");
		log.info("HomePageTitleTest");
		logger.log(LogStatus.INFO, "Loading HomePage");
		log.info("Loading Homepage");
		String title = homepage.verifyHomePageTitle();
		logger.log(LogStatus.INFO, "Verifying HomePage Title");
		log.info("Verifying HomePage Title");
		Assert.assertEquals(title, "VibgyorFeb18Client");
		logger.log(LogStatus.PASS, "Title Verified");
		log.info("Title Verified");
	}

	@Test(priority=5)
	public void vibgyorpaintlabelTest()
	{
		logger=report.startTest("HomePage vibgyor paintTest label test");
		log.info("HomePage vibgyor paintTest label test");
		logger.log(LogStatus.INFO, "Loading HomePage");
		log.info("Loading HomePage");
		logger.log(LogStatus.INFO, "Verifying HomePage Label");
		log.info("Verifying HomePage Label");

		try {
			Assert.assertTrue(CommonMethod.islabelpresent(kk.get(i)));
		}  catch (ElementNotClickable e) {
			// TODO Auto-generated catch block
			log.fatal(e.getMessage());
		} catch (ElementNotPresentException e) {
			// TODO Auto-generated catch block
			log.fatal(e.getMessage());
		} catch (LocatorNotFound e) {
			log.fatal("unable to find element");
			logger.log(LogStatus.FATAL, "Unable to find element");

		}catch(Exception e)
		{
			log.fatal(e.getMessage());
		}

		logger.log(LogStatus.PASS, "Label Verified");
		log.info("Label Verified");
	}
	@Test(priority=6)
	public void loginlabelTest()
	{
		logger=report.startTest("HomePage login label Test");
		log.info("HomePage login label Test");
		logger.log(LogStatus.INFO, "Loading HomePage");
		log.info("Loading HomePage");
		logger.log(LogStatus.INFO, "Verifying HomePage LoginLabel");
		log.info("Verifying HomePage LoginLabel");
		i++;
		try {
			Assert.assertTrue(CommonMethod.islabelpresent(kk.get(i)));
		} catch (ElementNotClickable e) {
			log.fatal(e.getMessage());
		} catch (ElementNotPresentException e) {
			log.fatal(e.getMessage());
		} catch (LocatorNotFound e) {
			log.fatal("unable to find element");
			logger.log(LogStatus.FATAL, "Unable to find element");

		}catch(Exception e)
		{
			log.fatal(e.getMessage());
		}

		logger.log(LogStatus.PASS, "LoginLabel Verified");
		log.info("LoginLabel Verified");
	}
	@Test(priority=7)
	public void signuplabelTest()
	{
		logger=report.startTest("HomePage signup label Test");
		log.info("HomePage signup label Test");
		logger.log(LogStatus.INFO, "Loading HomePage");
		log.info("Loading HomePage");
		logger.log(LogStatus.INFO, "Verifying HomePage SignUpLabel");
		log.info("Verifying HomePage SignUpLabel");
		i++;
		try {
			Assert.assertTrue(CommonMethod.islabelpresent(kk.get(i)));
		} catch (ElementNotClickable e) {
			log.fatal(e.getMessage());
		} catch (ElementNotPresentException e) {
			log.fatal(e.getMessage());
		} catch (LocatorNotFound e) {
			log.fatal("unable to find element");
			logger.log(LogStatus.FATAL, "Unable to find element");

		}catch(Exception e)
		{
			log.fatal(e.getMessage());
		}

		logger.log(LogStatus.PASS, "SignUpLabel Verified");
	}



	@AfterMethod
	public void tearDown(ITestResult result)
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			String imagePath=ScreenShot.captureScreenShot(driver, result.getName());
			String image=logger.addScreenCapture(imagePath)+"Some error occurred in home page";
			logger.log(LogStatus.ERROR, image);
			log.fatal("Some error occurred in home page");

		}
		report.endTest(logger);
		report.flush();

		driver.quit();
	}


}
